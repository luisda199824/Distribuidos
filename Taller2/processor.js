// Librería para obtener datos por consola
const readlineSync = require('readline-sync');
// Librería de sockets
const io = require('socket.io-client');

// Funciones desde utils
const getPORT = require('./utils.js').getPORT;
const doProcess = require('./utils.js').doProcess;
const inputNumber = require('./utils.js').inputNumber;

// Set port by default
let PORT = getPORT(process.argv, "FUNCTION PROCESSOR");

// Inicializar variable PROCESS
var PROCESS = null;

// Server WS
// Creación de servidor http
const server = require('http').createServer();
// Creación del socket dentro del servidor
const serverSocket = require('socket.io')(server, {});

// Proceso del servidor en el puerto PORT
server.listen(PORT, () => {
  console.log(`Server DNS up on port ${PORT}`);
});

// Manejo del socket del servidor
// Conexión de un cliente
serverSocket.on('connect', socket => {
  // Si un cliente se desconecta
  socket.on('disconnect', () => {
    // Mensaje de notificación de desconexión de cliente
    console.log(`Client ${socket.id} disconnected`);
  });

  // Mensaje de notificación de conexión de cliente
  console.log(`Client ${socket.id} connected`);

  // Recepción de solicitud de procesamiento
  socket.on('DO PROCESS', (num1, num2) => {
    // Envío de resultado de realizar operación al cliente
    socket.emit('PROCESS RESULT', doProcess(PROCESS, num1, num2));
  });
});

// Processor server
// Solicitar el puerto del servidor DNS
var DNS_PORT_SERVER = readlineSync.question('DNS PORT SERVER: ');
// Crear el socket (conexión) con servidor DNS
const DNS_SOCKET = io(`http://127.0.0.1:${DNS_PORT_SERVER}/`);
// Enviar evento para obtener los procesos disponibles
DNS_SOCKET.emit('GET AVAIBLE PROCESS TO FUNCTION PROCESSOR');

// Recepción de los procesos disponibles
DNS_SOCKET.on('AVAIBLE PROCESS TO FUNCTION PROCESSOR', avaibleServices => {
  // Recorrer la lista de procesos disponibles
  for (var index = 0; index < avaibleServices.length; index++) {
    // Imprimir indice con servicio
    console.log(`${index}. ${avaibleServices[index]}`);
  }

  // Capturar número del proceso a realizar
  var option = inputNumber('PROCESS TO DO: ');

  // Asignar proceso seleccionado a PROCESS
  PROCESS = avaibleServices[option];
  // Solicitud de asignación de proceso a este servidor
  DNS_SOCKET.emit('init process', PORT, PROCESS);
});

// Recepción de notificación de proceso asignado
DNS_SOCKET.on('process assigned', () => {
  // Mensaje de notificación
  console.log('PROCESS ASSIGNED');
});
