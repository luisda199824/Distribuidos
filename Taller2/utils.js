// Librería para obtener datos por consola
const readlineSync = require('readline-sync');

// Obtener el puerto a partir de la cadena port=XXXX
let getPORT = (argvs, typeConnect) => {
  // Inicialización de valPORT
  let valPORT = null;
  // Toma de los argumentos que vienen desde terminal
  process.argv.forEach(function (val, index, array) {
    // Si el argumento (val) incluye la palabra port
    if (val.includes('port')) {
      // Se settea valPORT con val
      valPORT = val;
    }
  });

  // Si port no fue encontrado entre los argumentos
  if (valPORT === null) {
    // Mensaje de error
    console.log(`NO PORT DEFINED ${typeConnect}`);
    // Finalización del proceso
    process.exit(1);
  }

  // Remover todos los elementos port de la cadena
  while (valPORT.includes('port')) {
    // Remplazo en la cadena con función replace
    valPORT = valPORT.replace('port', '');
  };
  // Remover todos los elementos ' ' (space) de la cadena
  while (valPORT.includes(' ')) {
    // Remplazo en la cadena con función replace
    valPORT = valPORT.replace(' ', '');
  };
  // Remover todos los elementos = de la cadena
  while (valPORT.includes('=')) {
    // Remplazo en la cadena con función replace
    valPORT = valPORT.replace('=', '');
  };

  // Retornar el número entero de valPORT
  return parseInt(valPORT);
};

// Función para obtener un número de una entrada por consola
// @param label : string : Mensaje que se muestra al solicitar valor por consola
let inputNumber = label => {
  // Obtener valor por consola
  var option = readlineSync.question(label);

  // Evaluar si es un número con re
  var isnum = /^\d+$/.test(option);

  // Si no es un número
  if (!isnum) {
    // Mensaje de error
    console.warn("ONLY DIGITS AVAIBLE");
    // Finalización del proceso
    process.exit(1);
  // Si es un número
  } else {
    // Retornar el número entero del valor ingresado
    return parseInt(option);
  }
}

// Realizar proceso
// @ PROCESS : string : Proceso a realizar
let doProcess = (PROCESS, op1, op2) => {
  // Mensaje de notificación
  console.log("--PROCESSING--");
  // Si el proceso es suma
  if (PROCESS === "suma") {
    // Mensaje de notificación
    console.log("--ENDED--");
    // Retornar la operación
    return op1+op2;
  // Si el proceso es resta
  } else if (PROCESS === "resta") {
    // Mensaje de notificación
    console.log("--ENDED--");
    // Retornar la operación
    return op1-op2;
  // Si el proceso es multiplicación
  } else if (PROCESS === "mult") {
    // Mensaje de notificación
    console.log("--ENDED--");
    // Retornar la operación
    return op1*op2;
  // Si el proceso es división
  } else if (PROCESS === "div") {
    // Mensaje de notificación
    console.log("--ENDED--");
    // Si el divisor no es 0
    if (op2 !== 0) {
      // Retornar la operación
      return op1/op2;
    // Si es 0, retornar 0
    } else return 0;
  }
};

// Retorno de modulos del archivo
module.exports = {
  getPORT,
  doProcess,
  inputNumber
};
