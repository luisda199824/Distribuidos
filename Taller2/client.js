// Función para obtener valor númerico por consola
const inputNumber = require('./utils.js').inputNumber;
// Librería de sockets
const io = require('socket.io-client');

// Inicializar variable PROCESS
var PROCESS = null;

// Solicitar el puerto del servidor DNS
var DNS_PORT_SERVER = inputNumber('DNS PORT SERVER: ');
// Crear el socket (conexión) con servidor DNS
const DNS_SOCKET = io(`http://127.0.0.1:${DNS_PORT_SERVER}/`);

// Enviar evento para obtener los procesos disponibles
DNS_SOCKET.emit('GET AVAIBLE PROCESS TO CLIENT');

// Recepción de los procesos disponibles
DNS_SOCKET.on('AVAIBLE PROCESS TO CLIENT', avaibleServices => {
  // Si no hay procesos disponibles
  if (avaibleServices.length === 0) {
    // Mensaje de notificación
    console.warn("ANY PROCESS AVAIBLE");
    // Finalización de cliente
    process.exit(1);
  }

  // Recorrer la lista de procesos disponibles
  for (var index = 0; index < avaibleServices.length; index++) {
    // Imprimir indice con servicio
    console.log(`${index}. ${avaibleServices[index]}`);
  }

  // Capturar número del proceso a realizar
  var option = inputNumber('PROCESS TO DO: ');

  // Asignar proceso seleccionado a PROCESS
  PROCESS = avaibleServices[option];
  // Obtener puerto de conección con servidor que realizar el proceso
  DNS_SOCKET.emit('GET DNS TO PROCESS', PROCESS);
});

// Recepción de evento de proceso ya no disponible
DNS_SOCKET.on("PROCESS DOESN'T EXISTS", () => {
  // Mensaje de notificación
  console.log("PROCESSOR DISCONNECTED");
  // Finalización de cliente
  process.exit(1);
});

// Recepción de puerto de servidor que realiza el proceso
DNS_SOCKET.on('DNS TO PROCESS', PROCESSOR_SOCKET => {
  // Mensaje de notificación con puerto de servidor que realiza el proceso
  console.log(`PORT: ${PROCESSOR_SOCKET}`);
  // Conexión con servidor que realiza el proceso
  var PROCESSOR_SOCKET = io(`http://127.0.0.1:${PROCESSOR_SOCKET}/`);
  // Obtener primer operando
  var op1 = inputNumber('First number to operate: ');
  // Obtener segundo operando
  var op2 = inputNumber('Second number to operate: ');

  // Solicitar la realización del proceso al servidor que realiza el proceso
  PROCESSOR_SOCKET.emit('DO PROCESS', op1, op2);

  // Recepción del resultado
  PROCESSOR_SOCKET.on('PROCESS RESULT', result => {
    // Mensaje de notificación con operación
    console.log(`${op1} ${PROCESS} ${op2} = ${result}`);
    // Cerrar conexión con el servidor que realiza el proceso
    PROCESSOR_SOCKET.close();
    // Mensaje de notificación
    console.warn("PROCESS COMPLETE");
    // Finalización de cliente
    process.exit();
  });

})
