// Creación de servidor http
const server = require('http').createServer();
// Librería de sockets y creación de socket
const io = require('socket.io')(server, {});
// Funciones desde utils
const getPORT = require('./utils.js').getPORT;

// Set port by default
let PORT = getPORT(process.argv, "DNS SERVER");
// Set unassignedProcess value by default
const unassignedProcess = {id: null, port: null};

// Inicializar variable dataServices
let dataServices = {
  suma: unassignedProcess,
  resta: unassignedProcess,
  mult: unassignedProcess,
  div: unassignedProcess
};

// Proceso del servidor en el puerto PORT
server.listen(PORT, () => {
  console.log(`Server DNS up on port ${PORT}`);
});

// Manejo del socket del servidor
// Conexión de un cliente
io.on('connect', socket => {
  // Si un cliente se desconecta
  socket.on('disconnect', () => {
    // Recorrer la lista de servicios
    for (var service in dataServices) {
      // Si el socket que se acaba de desconectar estaba asignado
      if (dataServices[service].id === socket.id) {
        // Se des-asigna el cliente que se desconectó y se deja libre el proceso
        dataServices[service] = unassignedProcess;
        // Mensaje de notificación de desconexión de cliente
        console.log(`PROCESS ${service} unassigned by socket disconnect`);
      }
    }
  });

  // Mensaje de notificación de conexión de cliente
  console.log(`Socket connected ${socket.id}`);

  // Recepción de solicitud de procesos disponibles para servidores procesadores
  socket.on('GET AVAIBLE PROCESS TO FUNCTION PROCESSOR', () => {
    // Inicializar variable avaibleServices con los servicios disponibles (no asignados)
    let avaibleServices = [];
    // Recorrer la lista de servicios
    for (var service in dataServices) {
      // Si el servicio no está asignado
      if (dataServices[service] === unassignedProcess) {
        // Se agrega a la lista de servicios disponibles
        avaibleServices.push(service);
      }
    }
    // Se envía al servidor procesador la lista de servicios disponibles
    socket.emit('AVAIBLE PROCESS TO FUNCTION PROCESSOR', avaibleServices);
  });

  // Solicitud de servidor procesador de tomar un proceso
  socket.on('init process', (port, processSelected) => {
    // Asignar al proceso la información del procesador que la tomó
    dataServices[processSelected] = {
      id: socket.id, port
    };
    // Enviar respuesta de proceso asignado
    socket.emit('process assigned');
    // Mensaje de notificación de proceso asignado
    console.log(`PROCESS ${processSelected} assigned to ${port}`);
  });

  // Recepción de solicitud de procesos disponibles para clientes
  socket.on('GET AVAIBLE PROCESS TO CLIENT', () => {
    // Inicializar variable avaibleServices con los servicios disponibles (asignados)
    let avaibleServices = [];
    // Recorrer la lista de servicios
    for (var service in dataServices) {
      // Si el proceso está asignado
      if (dataServices[service] !== unassignedProcess) {
        // Se agrega a la lista de servicios disponibles
        avaibleServices.push(service);
      }
    }

    // Se envía al cliente la lista de servicios disponibles
    socket.emit('AVAIBLE PROCESS TO CLIENT', avaibleServices);
  });

  // Cliente solicita la dirección de un proceso selecionado
  socket.on('GET DNS TO PROCESS', processSelected => {
    // Si existe el proceso que está solicitando el cliente
    if (dataServices.hasOwnProperty(processSelected)) {
      // Enviar respuesta con el puerto de conexión al servidor procesador
      socket.emit('DNS TO PROCESS', dataServices[processSelected].port);
    // Si no existe el proceso que el cliente busca
    } else {
      // Enviar notificación de error al cliente
      socket.emit("PROCESS DOESN'T EXISTS", processSelected)
    }
  });

});
