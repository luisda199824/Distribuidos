// Creación de servidor http
const server = require('http').createServer();
// Librería de sockets y creación de socket
const io = require('socket.io')(server, {});
// Funciones desde utils
const getPORT = require('./utils.js').getPORT;
// Funciones desde calc.js
const calcNewMean = require('./calc.js');

// Set port by default
let PORT = getPORT(process.argv, "CENTRAL SERVER");
// Initialization variables NUM_CLIENTS
var NUM_CLIENTS = 0;
// Initialization variables DATES
var DATES = [];

// Manejo del socket del servidor
// Conexión de un cliente
io.on('connect', socket => {
  // Mensaje de notificación de conexión de cliente
  console.log(`Socket connected ${socket.id}`);
  // Increment clients number
  NUM_CLIENTS += 1;

  // Si un cliente se desconecta
  socket.on('disconnect', () => {
    // Mensaje de notificación
    console.log(`Client ${socket.id} disconnect`);
    // Reducir número de clientes
    NUM_CLIENTS -= 1;
  });

  // Recepción de una fecha de un cliente
  socket.on('TAKE DATE', date =>  {
    // Agregar la fecha al conjunto de fechas enviadas por los clientes
    DATES.push(new Date(date));
    // Mensaje de notificación
    console.log(`DATE RECEIVED FROM ${socket.id}`);

    // Si todos los usuarios ya enviaron la fecha, hacer un balance
    if (DATES.length === NUM_CLIENTS) {
      // Se calcula la nueva fecha respecto a las fechas enviadas
      newMeanDate = calcNewMean(DATES);
      // Se restaura al estado inicial (vacío) la variable DATES
      DATES = [];
      // Se envía un broadcast con la nueva fecha
      io.emit('NEW DATE', new Date(newMeanDate));
    }
  });
});

// Proceso del servidor en el puerto PORT
server.listen(PORT, () => {
  // Mensaje de notificación
  console.log(`Server CENTRAL up on port ${PORT}`);

  // Cada 30 segundos se pedirá la fecha de cada cliente
  setInterval(() => {
    // Broadcast de solicitud de hora
    io.emit('SEND DATE');
  }, 30000);

});
