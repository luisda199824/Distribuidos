// load math.js
var math = require('mathjs');

// Función para separar las fechas validas de las fechas que se alejan de la media
// @param values : array : dates in milliseconds
// @param mean : float : dates mean
// @param std : float : dates standard desviation
let separateDates = (values, mean, std) => {
  // Define min and max date values
  let minDate = mean - std;
  let maxDate = mean + std;

  // Inicializar el array de valores válidos
  let validValues = [];

  // Recorrer la lista de dates
  for (var index = 0; index < values.length; index++) {
    // Inicializar la variable value (date in milliseconds)
    let value = values[index];

    if (
      // Si date es mayor o igual al valor mínimo
      (value >= minDate) &&
      // Y menor o igual al valor máximo
      (value <= maxDate)
    ) {
      // Es una fecha dentro del rango
      // Es una fecha válida
      // Se agrega al array de valores válidos
       validValues.push(value);
    } else {
      // No se encuentra dentro de los valores válidos
      // Mensaje de notificación de oviación de la fecha
      console.log(`DATE NOT TAKED ${value}`);
    }
  }

  // Retorno de los valores válidos
  return values;
}

// Función para calcular la nueva fecha promedio
let calcNewMean = dates => {
  // Se inicializa el array que contendrá las fechas en milliseconds
  let values = [];
  // Se recorre el array de fechas
  for (var index = 0; index < dates.length; index++) {
    // Se convierten en milliseconds (dates[index].getTime())
    // Y se agrega a la lista de valores
    values.push(dates[index].getTime());
  }

  // Se halla la standard desviation de los valores en milliseconds
  std = math.std(values);
  // Se halla el promedio de los valores en milliseconds
  mean = math.mean(values);

  // Se hallan los valores válidos para sacar el promedio
  let validDates = separateDates(values, mean, std);

  // Retorno de la nueva media de las fechas válidas
  return math.mean(validDates);
}

// Exportación del módulo
module.exports = calcNewMean;
