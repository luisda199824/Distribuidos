// Función para obtener valor númerico por consola
const inputNumber = require('./utils.js').inputNumber;
// Librería de sockets
const io = require('socket.io-client');

// Solicitar el puerto del servidor DNS
var DNS_PORT_SERVER = inputNumber('DNS PORT SERVER: ');
// Crear el socket (conexión) con servidor DNS
const DNS_SOCKET = io(`http://127.0.0.1:${DNS_PORT_SERVER}/`);

var TYPE_DATE = null;
var DATE = new Date();

// Recepción de los procesos disponibles
DNS_SOCKET.on('SEND DATE', () => {
  console.log("Options: ");
  console.log("1. Simulated date");
  console.log("2. System date");
  var option = inputNumber('Select an option: ');

  TYPE_DATE = (option === 1) ? "simulated" : "system date";

  if (TYPE_DATE === "simulated") {
    let seconds = inputNumber('ENTER A SECONDS: ');
    let minutes = inputNumber('ENTER A MINUTES: ');
    let hours = inputNumber('ENTER A HOURS: ');
    let day = inputNumber('ENTER A DAY: ')-1;
    let month = inputNumber('ENTER A MONTH: ')-1;
    let year = inputNumber('ENTER A YEAR: ');
    DATE = new Date(year, month, day, hours, minutes, seconds, 0);

    DNS_SOCKET.emit('TAKE DATE', DATE);
  } else {
    DATE = new Date();
    DNS_SOCKET.emit('TAKE DATE', DATE);
  }
});

DNS_SOCKET.on('NEW DATE', date => {
  console.log(`NEW DATE FROM CENTRAL SERVER ${date}`);
});
