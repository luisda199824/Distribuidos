// Function to remove a client from the table db
// @param : TABLE : JSONObject : Actual Table to be modified
// @param : socketID : string (ID) : Client to remove
let removeClientInTable = (TABLE, socketID) => {

  // Delete client row from the table db
  delete TABLE[socketID];

  // Define all the clientsID avaliables
  let clientsID = Object.keys(TABLE);

  // Loop for all the client rows
  for (var index = 0; index < clientsID.length; index++) {
    // Get clientID in list
    let clientID = clientsID[index];

    // Files in this client
    let fileNames = Object.keys(TABLE[clientID].files);

    // Loop for all files of this client
    for (var indexFiles = 0; indexFiles < fileNames.length; indexFiles++) {
      // Get fileName
      let file = fileNames[indexFiles];
      // Remove socketID (removed client) permissions in client file
      try {
        delete TABLE[clientID].files[file].permissions[socketID];
      } catch (e) {
        console.log(`Error deleting permissions`);
      }
    }
  }

  // Return new table db
  return TABLE;
}

// Module export function removeClientInTable
module.exports = removeClientInTable;
