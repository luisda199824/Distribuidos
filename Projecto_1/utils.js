// Librería para obtener datos por consola
const readlineSync = require('readline-sync');

// Function to get random permissions
let getRandomPermissions = () => {
  // Define write permissions
  // Math.floor(Math.random()*2) return a value (0 or 1)
  // If value returned is 1, a permissions is able, else not
  let write = Math.floor(Math.random()*2) === 1;
  // Read permissions:
  // If has write permissions, has read permissions, else, it's get a random

  // Return a permissions object content
  return {
    write,
    read: write ? write : Math.floor(Math.random()*2) === 1
  };
};

// Function to get a string to show a permissions abled
// @param: permissions : JSONObject : Permission content
let getPermissions = permissions => {
  // Define a string to return a string
  // If has write permission, add "w", else, add an empty string
  var string = permissions.write ? "w" : "";
  // If has rid permission, add "r", else, add an empty string
  string += permissions.read ? "r" : "";

  // If not has permission
  if (string === "") {
    // Return NA
    return "NA";
  } else {
    // If has some permission, return string created
    return string;
  }
};

// Función para obtener una cadena de entrada por consola
// @param label : string : Mensaje que se muestra al solicitar valor por consola
let input = label => {
  // Obtener valor por consola
  var option = readlineSync.question(label);

  return option;
};

// Función para obtener un número de una entrada por consola
// @param label : string : Mensaje que se muestra al solicitar valor por consola
let inputNumber = label => {
  // Obtener valor por consola
  var option = readlineSync.question(label);

  // Evaluar si es un número con re
  var isnum = /^\d+$/.test(option);

  // Si no es un número
  if (!isnum) {
    // Mensaje de error
    console.warn("ONLY DIGITS AVAIBLE");
    // Finalización del proceso
    process.exit(1);
  // Si es un número
  } else {
    // Retornar el número entero del valor ingresado
    return parseInt(option);
  }
};

// Librería para manejo de archivos y directorios
const fs = require('fs');

// Función para generar un log en el archivo log/log.txt
// @param action : string : Identifica si mensaje fue leído (readed),
//                modificado (modified) o borrado (deleted),
//                o se requiere inicializar el servidor (init)
// @param byClient : string : ID del cliente que ejecutó la acción
// @param file : string : Nombre del archivo afectado
let createLog = (action, byClient, file) => {

  // Validación de que la carpeta log existe
  // Si no existe
  if (!fs.existsSync('./log/')) {
    // Crear carpeta log
    fs.mkdirSync('./log/');
  }

  // Validar archivo log existe
  // Si no existe
  if (!fs.existsSync('./log/')) {
    // Crear archivo log vacío
    fs.createWriteStream('./log/log.txt');
  }

  // Generate new time stamp
  var date = new Date();
  // Generate vars time stamp
  let day = date.getDate(), month = date.getMonth(), year = date.getFullYear(),
    hour = date.getHours(), minute = date.getMinutes(), seconds = date.getSeconds();

  // Generate string stamp
  var stamp = `${byClient ? byClient : "Init" } - ${day}/${month}/${year} ${hour}:${minute}:${seconds} - `, message = "\n";

  if (
    (action === "init")
  ) {
    message += `\n${stamp}Server updated - HTTP/101`;
  } else if (
    (action === "readed")
    || (action === "modified")
    || (action === "deleted")
  ) {
    message += `${stamp}file ${file} ${action} - HTTP/101`;
  } else {
    return false;
  }

  fs.appendFile('./log/log.txt', message, function (err) {
    if (err) {
      throw err;
    } else {
      return true
    };
  });
}

let getAddPortClientSocket = (clientID, TABLE, CLIENTS) => {
  return {
    address: CLIENTS[clientID].request.connection._peername.address.replace("::ffff:", ""),
    port: TABLE[clientID].port,
    clientID: clientID
  };
}

let getTwoCopyClients = (TABLE, CLIENTS, clientID) => {
  var copyClients = [], clientsIDs = Object.keys(CLIENTS);
  if (clientsIDs.length === 3) {
    for (var indexClientsCopy = 0; indexClientsCopy < clientsIDs.length; indexClientsCopy++) {
      if (clientsIDs[indexClientsCopy] !== clientID) {
        copyClients.push(getAddPortClientSocket(clientsIDs[indexClientsCopy], TABLE, CLIENTS));
      }
    }
  } else {
    while (true) {
      let num1 = Math.floor(Math.random()*clientsIDs.length),
        num2 = Math.floor(Math.random()*clientsIDs.length);

      if (
        (num1 !== num2)
        && (CLIENTS[clientsIDs[num1]] !== clientID)
        && (CLIENTS[clientsIDs[num2]] !== clientID)
      ) {
        copyClients = [
          getAddPortClientSocket(clientsIDs[num1], TABLE, CLIENTS),
          getAddPortClientSocket(clientsIDs[num2], TABLE, CLIENTS)
        ];
        break;
      }
    }
  }
  return copyClients;
}

let doCopy = (TABLE, CLIENTS, clientID) => {
  const tempTABLE = TABLE, tempCLIENTS = CLIENTS;
  var copyClients = getTwoCopyClients(tempTABLE, tempCLIENTS, clientID);

  copyFiles = TABLE[clientID].files;
  files = Object.keys(TABLE[clientID].files);
  for (var indexFiles = 0; indexFiles < files.length; indexFiles++) {
    urlClientCopy1 = `http://${copyClients[0].address}:${copyClients[0].port}`;
    clientID1 = copyClients[0].clientID;
    urlClientCopy2 = `http://${copyClients[1].address}:${copyClients[1].port}`;
    clientID2 = copyClients[1].clientID;

    CLIENTS[clientID].emit('send files copy to', urlClientCopy1, clientID1,  urlClientCopy2, clientID2, "creation");

    TABLE[clientID].files = copyFiles;
    TABLE[clientID].copies = { clientID1, clientID2 };
  }

  return TABLE;
}

let doCopyFilesAllMembers = (TABLE, CLIENTS) => {
  let clientsIDS = Object.keys(CLIENTS);
  for (var index = 0; index < clientsIDS.length; index++) {
    clientID = clientsIDS[index];
    TABLE = doCopy(TABLE, CLIENTS, clientID);
  }

  return TABLE;
}

let reviewCopies = (TABLE, clientID, CLIENTS) => {
  var validClients = [];

  for (var index = 0; index < Object.keys(TABLE).length; index++) {
    if (TABLE[Object.keys(TABLE)[index]].port) {
      validClients.push(TABLE[Object.keys(TABLE)[index]]);
    }
  }

  if (validClients.length === 3) {
    TABLE = doCopyFilesAllMembers(TABLE, CLIENTS);
  } else if (validClients.length > 3) {
    TABLE = doCopy(TABLE, CLIENTS, clientID);
  } else {
    // Wait for others clients
  }

  return TABLE;
}

// Module exports a object that contains three functions:
// @function: getPermissions
// @function: inputNumber
// @function: getRandomPermissions
module.exports = {
  createLog,
  getPermissions,
  inputNumber,
  getRandomPermissions,
  input,
  reviewCopies,
  getAddPortClientSocket
};
