// Librería para manejo de archivos y directorios
const fs = require('fs');

const fetch = require('node-fetch');

let getFileContent = (fileName, clientFolder) => {
  return fs.readFileSync(`./${clientFolder}/${fileName}`, "utf8");
};

let sendFilesCopyTo = (FILES, url, clientID, ownerID, FOLDER_CLIENT, action, fileName, clientOwner) => {
  var data = {};
  if (action === "creation") {
    data = {ownerID, files: []};

    for (var index = 0; index < FILES.length; index++) {
      file = {fileName: FILES[index], content: getFileContent(FILES[index], FOLDER_CLIENT)};
      data.files.push(file);
    }
  } else if (action === "modified") {
    data = {ownerID, fileName, content: getFileContent(fileName, FOLDER_CLIENT)};
  } else if (action === "delete") {
    data = {ownerID: clientOwner, fileName};
  }

  fetch(`${url}?data=${JSON.stringify(data)}`, { method: 'POST'})
    .then(res => res.text())
    .then(body => {});

}

let deleteFileCopy = (ownerID, fileName, PORT_SERVER) => {
  if (fs.existsSync(`./copies-${PORT_SERVER}/${ownerID}/${fileName}`)) {
    fs.unlinkSync(`./copies-${PORT_SERVER}/${ownerID}/${fileName}`);
  }
}

let createFileCopy = (ownerID, fileName, content, PORT_SERVER) => {
  if (fs.existsSync(`./copies-${PORT_SERVER}/${ownerID}/${fileName}`)) {
    deleteFileCopy(ownerID, `./copies-${PORT_SERVER}/${ownerID}/${fileName}`, PORT_SERVER);
  }

  try {
      if (!fs.existsSync(`./copies-${PORT_SERVER}/${ownerID}/`)) {
        fs.mkdirSync(`./copies-${PORT_SERVER}/${ownerID}/`);
      }
  } catch (e) {
    // Nothing
  }

  const { exec } = require('child_process');
  exec(`touch ./copies-${PORT_SERVER}/${ownerID}/${fileName}`, (err, stdout, stderr) => {
    if (err) {
      console.error(err);
      return;
    } else {
      fs.writeFileSync(`./copies-${PORT_SERVER}/${ownerID}/${fileName}`, content);
      console.log(`created copies-${PORT_SERVER}/${ownerID}/${fileName}`);
    }
    console.log(stdout);
  });

}

let createCopies = (data, PORT_SERVER) => {
  let {
    ownerID,
    files
  } = data;

  if (!fs.existsSync(`./copies-${PORT_SERVER}`)) {
    fs.mkdir(`./copies-${PORT_SERVER}`, () => {});
  }

  if (!fs.existsSync(`./copies-${PORT_SERVER}/${ownerID}`)) {
    fs.mkdir(`./copies-${PORT_SERVER}/${ownerID}`, () => {});
  }

  for (var indexFile = 0; indexFile < files.length; indexFile++) {
    file = files[indexFile];
    createFileCopy(ownerID, file.fileName, file.content, PORT_SERVER);
  }
}

module.exports = {
  sendFilesCopyTo,
  createCopies,
  deleteFileCopy,
  createFileCopy
};
