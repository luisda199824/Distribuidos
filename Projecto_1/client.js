// load path library to resolve all dirs on os
const path = require('path');
// Require express to server
const express  = require('express');
// Create express app
var app = express();
// Create httpServer with app expresss
var http = require('http').Server(app);
// Librería de sockets y creación de socket
var ioServer = require('socket.io')(http);
// Librería de sockets
var ioClient = require('socket.io-client');
// Librerìa del sistema
const fs = require('fs');
// Librerìa para abrir navegador web
const openBrowserInPage = require('opn');
// Función para obtener valor númerico por consola
const inputNumber = require('./utils.js').inputNumber;
// Función para obtener cadena por consola
const input = require('./utils.js').input;
// Función para enviar fileContent
const sendFilesCopyTo = require('./clientUtils.js').sendFilesCopyTo;
const createCopies = require('./clientUtils.js').createCopies;
const createFileCopy = require('./clientUtils.js').createFileCopy;
const deleteFileCopy = require('./clientUtils.js').deleteFileCopy;

// Inicializate IDClient, dirClient Folder, Client files and Socket;
var ID = -1,
  FOLDER_CLIENT_DIR = '',
  FILES = [],
  SOCKET = null;

// Solicitar el puerto del servidor FILES
var FILES_PORT_SERVER = input('FILE IP SERVER: ');

// Solicitar el puerto del servidor
var PORT_SERVER = inputNumber('PORT SERVER: ');

let clientFolder = `client-${PORT_SERVER}`;

// Crear el socket (conexión) con servidor FILES
const FILES_SOCKET = ioClient(`http://${FILES_PORT_SERVER}/`);

// Use Uri("/assets/") like static files into assets folder
app.use('/assets/', express.static('assets'))
// Define Uri path ("/") to serve client html file
app.get('/', (req, res) => {
  // Response with file client.html
  res.sendFile(path.resolve('templates/client.html'))
});

app.post('/:action/copy/:clientID', (req, res) => {
  // console.log(req.params.clientID, req.params.action, req.query, typeof(req.query), typeof(req.query.data));
  data = JSON.parse(req.query.data);
  
  if (req.params.action === "creation") {
    createCopies(data, PORT_SERVER);
    res.json({ok: "ok"}).status(200);
  } else if (req.params.action === "modified") {
    createFileCopy(data.ownerID, data.fileName, data.content, PORT_SERVER)
    res.json({ok: "ok"}).status(200);
  } else if (req.params.action === "delete") {
    deleteFileCopy(data.ownerID, data.fileName, PORT_SERVER);
    res.json({ok: "ok"}).status(200);
  } else {
    res.json({err: "not valid operation"}).status(400);
  }

});

// Creation of socket connection
ioServer.on('connect', socket => {
  // Save socket into SOCKET global variable
  SOCKET = socket;

  // Mensaje de notificación de cliente conectado
  console.log(`Client ${socket.id} connected`);

  // Emit to HTML Client the ID on FILES_SERVER
  socket.emit('id', ID);

  // Send files in client folder to FILES_SERVER
  socket.on('send files to server', () => {
    // Read files on client folder
    fs.readdirSync(FOLDER_CLIENT_DIR).forEach(fileName => {
      // Add file name to FILES global list
      FILES.push(fileName);
    });

    // If the folder doesn't have files
    if (FILES.length === 0) {
      // Send error message
      socket.emit('filesdir empty');
    }

    // Send files to FILES SERVER
    FILES_SOCKET.emit('list files', FILES, PORT_SERVER);
  });

  // Event to get the file's content
  // @param: fileName : string : File's name to get content
  // @param: clientOwner : string (ID) : File's Owner ID socket on FILES SERVER
  // @param: toEdit : boolean : Identify if the request is to get the content
  // to edit it or not
  socket.on('get file content', (fileName, clientOwner, toEdit) => {
    // Send request to FILES SERVER to connect with file's owner
    FILES_SOCKET.emit('get file content', fileName, clientOwner, toEdit);
  });

  // Close file when ends reading/writting
  // @param: fileName : string : File's name to close reading/writting
  // @oaram: clientOwner : string (ID) : File's Owner ID socket on FILES SERVER
  socket.on('closed file', (fileName, clientOwner) => {
    // Send request to FILES SERVER to close the file
    FILES_SOCKET.emit('closed file', fileName, clientOwner);
  });

  // Save content to file wrote
  // @param: fileName : string : File's name to save
  // @oaram: clientOwner : string (ID) : File's Owner ID socket on FILES SERVER
  // @param: content : Buffer : Content file to save
  socket.on('save content file', (fileName, clientOwner, content) => {
    // Send request to FILES SERVER to connect with file's owner and save the
    // new content
    FILES_SOCKET.emit('save content file', fileName, clientOwner, content);

    // TODO: Update file
  });

  // Delete file
  // @param: fileName : string : File's name to delete
  // @oaram: clientOwner : string (ID) : File's Owner ID socket on FILES SERVER
  socket.on('delete file', (fileName, clientOwner) => {
    // Send request to FILES SERVER to connect with file's owner to him delete it
    FILES_SOCKET.emit('delete file', fileName, clientOwner);

    // TODO: Delete copies file
  })
});

// Open http server on PORT_SERVER defined by user
http.listen(PORT_SERVER, () => {
  // Server up's notification message
  console.log(`Client up on port ${PORT_SERVER}`);
});

// Event connection success to FILES SERVER SOCKET
// @params : id : string (ID) : ID Client in FILES SERVER
FILES_SOCKET.on('client initializated', id => {
  // Save id param on ID global variable
  ID = id;

  // Define client folder like "client-ID"
  FOLDER_CLIENT_DIR = `./${clientFolder}`;
  // Make client folder
  // @param: callback : function : Notify correct client folder creation
  if (!fs.existsSync(`./${clientFolder}`)) {
    fs.mkdir(FOLDER_CLIENT_DIR, () => console.log('Folder created'));
  }

  // Open default browser in client page (Defined by PORT_SERVER)
  openBrowserInPage(`http://127.0.0.1:${PORT_SERVER}/`);
});

// Files tabled updated : Notification files are updated
// @param : tableFiles : JSONObject : Contains all information about
// files, its owners and self-permissions
// @param: clientIDUpdated : string (ID) : ID Client updated on FILES SERVER
FILES_SOCKET.on('files updated', (tableFiles, clientIDUpdated) => {
  // Notify to browser or client to update files table
  ioServer.emit('files updated', FILES.length, tableFiles, clientIDUpdated);
})

// Notification of another client request a file's content
// @param : fileName : string : File's name to send a content
// @param : clientID : string (ID) : Client's ID request the file's content
// @param : toEdit : boolean : Identify if content'll be modified
FILES_SOCKET.on('send content file', (fileName, clientID, toEdit) => {
  // Read file with name fileName in client folder
  // @param : filePath : string : folderClient + / + fileName
  // @param : callback : function : Callback reaction to file's content
  fs.readFile(`./${clientFolder}/${fileName}`, (err, content) => {
    // If not exists error
    if (!err) {
      // Notifiy to FILES SERVER to send file's content to client requested
      // And return again if this content is to be modified
      FILES_SOCKET.emit('content file', fileName, clientID, content, toEdit);
    }
  });
});

// Notify of response to request content file to another client
// @param: fileName : string : File's name content required
// @param: clientOwner : string (ID) : ID on FILES SERVER owner client
// @param: content : Buffer : File's content
FILES_SOCKET.on('content file', (fileName, clientOwner, content) => {
  // Send to browser or client the file content
  ioServer.emit('content file', fileName, clientOwner, content);
});

// Notification file in use by another client
// @param: editorID : string (ID) : Client ID using the content file (writting)
FILES_SOCKET.on('file in use', editorID => {
  // Notify to browser or client that files is using in this moment
  ioServer.emit('file in use', editorID);
})

// Notification file saved correctly
// @param: fileName : string : File's name saved correctly
FILES_SOCKET.on('file saved', fileName => {
  // Notify to browser or client that file did save correctly
  ioServer.emit('file saved', fileName);
})

// Notification to update a file by another client
// @param: fileName : string : File's name to change
// @param: content : Buffer : File's new content
// @param: editorID : string (ID) : Client who made the update
FILES_SOCKET.on('update file', (fileName, content, editorID) => {
  // Verify if file exists
  // If file exists
  if (fs.existsSync(`./${clientFolder}/${fileName}`)) {
    // Delete a file
    fs.unlinkSync(`./${clientFolder}/${fileName}`);
    // Create a file with a new content
    // @param: path new file : string
    // @param: content : Buffer/string : new content
    // @param: callback : function : error or callback function
    fs.writeFile(`./${clientFolder}/${fileName}`, content, err => {
      // If some error ocurred
      if (err) {
        // Notify to FILES SERVER that file can't be changed
        FILES_SOCKET.emit('file not exists', fileName, editorID);
        // End function
        return;
      // If not error ocurred
      } else {
        // Notify to FILES SERVER to notify editorClient that file be changed
        FILES_SOCKET.emit('notify file saved', fileName, editorID);
      }
    });
  // If file doesn't exists
  } else {
    // Notify to FILES SERVER that file can't be changed
    FILES_SOCKET.emit('error saving file', fileName, editorID, 'file not found');
  }
});

// Notification to delete a file by another client
// @param: fileName : string : File's name to delete
// @param: clientToNotify : string (ID) : ID on FILES SERVER to
// client who made a delete request
FILES_SOCKET.on('delete file', (fileName, clientToNotify) => {
  // Validate if file exists
  if (fs.existsSync(`./${clientFolder}/${fileName}`)) {
    // Delete a file
    fs.unlinkSync(`./${clientFolder}/${fileName}`);
    // Notify to FILES SERVER that file be removed
    FILES_SOCKET.emit('file deleted', fileName, clientToNotify);
  } else {
    // Notify to FILES SERVER that file can't be removed
    FILES_SOCKET.emit('error deleting file', fileName, clientToNotify, 'file not found');
  }
});

// Notification of file deleted from another client
// @param: fileName : string : File's name deleted
FILES_SOCKET.on('file deleted', fileName => {
  // Notify to browser or client that file be removed
  ioServer.emit('file deleted', fileName);
});

// Notification of file not be deleted from another client
// Usually by file in use by editor client
// @param: fileName : string : File's Name not deleted
// @param: editorID : string (ID) : Client who's using file
FILES_SOCKET.on('file not deleted', (fileName, editorID) => {
  // Notify to browser or client that file can't be removed
  ioServer.emit('file not deleted', fileName, editorID);
});

FILES_SOCKET.on('send files copy to', (urlClientCopy1, clientID1,  urlClientCopy2, clientID2, action, fileName, clientOwner) => {
  sendFilesCopyTo(FILES, `${urlClientCopy1}/${action}/copy/${ID}`, clientID1, ID, FOLDER_CLIENT_DIR, action, fileName, clientOwner);
  sendFilesCopyTo(FILES, `${urlClientCopy2}/${action}/copy/${ID}`, clientID2, ID, FOLDER_CLIENT_DIR, action, fileName, clientOwner);
});
