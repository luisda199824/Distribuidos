// Creación de servidor http
const server = require('http').createServer();
// Librería de sockets y creación de socket
const io = require('socket.io')(server, {});
// Funciones propias
// Función para remover cliente de la tabla principal
const removeClientInTable = require('./removeClientInTable.js');
// Función para generar los permisos sobre los archivos
const updatePermissions = require('./updatePermissions.js');
// Función para obtener los permisos a string
const getPermissions = require('./utils.js').getPermissions;
// Función para crear log
const createLog = require('./utils.js').createLog;
// Función para revisar copias
const reviewCopies = require('./utils.js').reviewCopies;
const getAddPortClientSocket = require('./utils.js').getAddPortClientSocket;

// Definición del puerto por defecto del servidor de archivos
let PORT = 3000;

// Generación del servidor
server.listen(PORT, () => {
  // Mensaje de notificación de servidor up
  console.log(`Files server up on port ${PORT}`);
  createLog("init", null, null);
});

// Inicialización de TABLE y CLIENTS
let TABLE = {}, CLIENTS = {};

// Creation of socket connection
io.on('connect', socket => {

  // Set socket to socket.id position to socket connection
  CLIENTS[socket.id] = socket;
  // Set empty files list to socket.id position
  TABLE[socket.id] = {
    files: {},
    port: null
  };

  // Message new user connected notification
  console.log(`Client ${socket.id} connected`);

  // On event disconnection client
  socket.on('disconnect', () => {
    // Message user disconnected notification
    console.log(`Client ${socket.id} disconnected`);
    // Remove client, files and permissions from main table
    TABLE = removeClientInTable(TABLE, socket.id);
    // Broadcast to update table files clients with idClient disconnected
    io.emit('files updated', TABLE, socket.id);
  });

  // Send to socket the ID assigned
  socket.emit('client initializated', socket.id);

  // Client send list files
  // @param: files : Arraylist : Files list on client folder
  socket.on('list files', (files, port_server) => {
    // If file list isn't empty
    if (files.length !== 0) {
      // Create row and permissions client in table on another client files
      TABLE = updatePermissions(TABLE, socket.id, files);
      TABLE[socket.id].port = port_server;
      // Broadcast to update table files clients with idClient connected
      io.emit('files updated', TABLE, socket.id);
    }

    reviewCopies(TABLE, socket.id, CLIENTS);
  });

  // Client send a content file to another client
  // @param: fileName : string : File name content required
  // @param: clientID : string (ID) : Client destination content
  // @param: content : Buffer : Content file sended
  // @param: toEdit : boolean : Identify if the content will be modified
  socket.on('content file', (fileName, clientID, content, toEdit) => {
    // Send to client destination, the file content, fileName, client responser
    CLIENTS[clientID].emit('content file', fileName, socket.id, content);
    // Create log
    createLog("readed", clientID, fileName);
  });

  // Client request a file content
  // @param: fileName : string : File name content required
  // @param: clientOwner : string (ID) : Client owner file
  // @param: toEdit : boolean : Identify if the content will be modified
  socket.on('get file content', (fileName, clientOwner, toEdit) => {
    // If client has the file
    if (TABLE[clientOwner].files.hasOwnProperty(fileName)) {
      // Verify if client (socket.id) has read permission
      if (TABLE[clientOwner].files[fileName].permissions[socket.id].read) {
        // If file will be modified
        if (toEdit) {
          // If not another client is using the file
          if (!TABLE[clientOwner].files[fileName].inUse) {
            // Notify file in use
            TABLE[clientOwner].files[fileName].inUse = true;
            // Reserve file use to client request
            TABLE[clientOwner].files[fileName].inUseBy = socket.id;
            // Notify to file clientOwner to request the file content
            CLIENTS[clientOwner].emit('send content file', fileName, socket.id, toEdit);
          // If another client is using the file
          } else {
            // Send to client request, the file is using by another client
            socket.emit('file in use', TABLE[clientOwner].files[fileName].inUseBy);
          }
        // If the file will not to modify
        } else {
          // Notify to file clientOwner to request the file content
          CLIENTS[clientOwner].emit('send content file', fileName, socket.id, toEdit);
        }
      // Verify if client (socket.id) hasn't read permission
      } else {
        // Send to client request, hasn't permission to read the file
        socket.emit('no authorized');
      }
    }
  });

  // Client
  // @param: fileName : string : File name to close
  // @param: clientOwner : string (ID) : Client owner file
  socket.on('closed file', (fileName, clientOwner) => {
    // Set file usable
    TABLE[clientOwner].files[fileName].inUse = false;
    // Set empty client using the file
    TABLE[clientOwner].files[fileName].inUseBy = null;
  });

  // Client want to save the content of file
  // @param: fileName : string : File name content to save
  // @param: clientOwner : string (ID) : Client owner file
  // @param: content : Buffer : New content file
  socket.on('save content file', (fileName, clientOwner, content) => {
    // Notify to file owner to update the file with new content
    // sended by client
    CLIENTS[clientOwner].emit(
      'update file', fileName, content, socket.id
    );
  });

  // Notify to client wrote new content file that file has saved correctly
  // @param: fileName : string : File name content saved
  // @param: clientToNotify : string (ID) : Client to notify file saved
  socket.on('notify file saved', (fileName, clientToNotify) => {
    // Notify to client that file has saved
    CLIENTS[clientToNotify].emit('file saved', fileName);
    // Create log
    createLog("modified", socket.id, fileName);

    copyClients = [
      getAddPortClientSocket(TABLE[socket.id].copies.clientID1, TABLE, CLIENTS),
      getAddPortClientSocket(TABLE[socket.id].copies.clientID2, TABLE, CLIENTS)
    ];

    // Update copies
    urlClientCopy1 = `http://${copyClients[0].address}:${copyClients[0].port}`;
    clientID1 = copyClients[0].clientID;
    urlClientCopy2 = `http://${copyClients[1].address}:${copyClients[1].port}`;
    clientID2 = copyClients[1].clientID;

    CLIENTS[socket.id].emit('send files copy to', urlClientCopy1, clientID1, urlClientCopy2, clientID2, "modified", fileName);
  });

  // Notify to client want to delete file
  // @param: fileName : string : Name file to delete
  // @param: clientOwner : string (ID) : Client to notify to delete the file
  socket.on('delete file', (fileName, clientOwner) => {
    // If the file is use by another client
    if (TABLE[clientOwner].files[fileName].inUse) {
      // Notify to client request that file is using by another client (inUseBy)
      socket.emit(
        'file not deleted', fileName, TABLE[clientOwner].files[fileName].inUseBy
      );
    // If the file isn't use by another client
    } else {
      // Notify to file owner to delete a file, sended too the client request ID
      CLIENTS[clientOwner].emit('delete file', fileName, socket.id);

      copyClients = [
        getAddPortClientSocket(TABLE[clientOwner].copies.clientID1, TABLE, CLIENTS),
        getAddPortClientSocket(TABLE[clientOwner].copies.clientID2, TABLE, CLIENTS)
      ];

      // Delete copies
      urlClientCopy1 = `http://${copyClients[0].address}:${copyClients[0].port}`;
      clientID1 = copyClients[0].clientID;
      urlClientCopy2 = `http://${copyClients[1].address}:${copyClients[1].port}`;
      clientID2 = copyClients[1].clientID;
      CLIENTS[socket.id].emit('send files copy to', urlClientCopy1, clientID1,  urlClientCopy2, clientID2, "delete", fileName, clientOwner);
    }
  });

  // File deleted notification
  // @param: fileName : string : File name deleted
  // @param: clientOwner : string (ID) : Client to notify to file deleted
  socket.on('file deleted', (fileName, clientToNotify) => {
    // Send to client requested, the file deleted notification with file name
    CLIENTS[clientToNotify].emit('file deleted', fileName);
    // Delete file from main table
    delete TABLE[socket.id].files[fileName];
    // Broadcast to update table files clients
    io.emit('files updated', TABLE, "Server");
    // Create log
    createLog("deleted", socket.id, fileName);
  });

});
