const getRandomPermissions = require('./utils.js').getRandomPermissions;

// Función para actualizar la tabla de archivos y los permisos de todos estos
// @param : TABLE : JSONObject : Tabla de archivos
// @param : socketID : int : ID del nuevo cliente de archivos
// @param : socketFiles : Array : Lista de archivos del nuevo cliente
let updatePermissionsFiles = (TABLE, socketID, socketFiles) => {

  // Obtener la lista con los ID de los clientes actuales de la tabla
  let clientsIDs = Object.keys(TABLE);

  // Asginación de los nuevos archivos del cliente en la tabla principal
  // Se recorre la lista de archivos del nuevo cliente [socketFiles]
  // con la variable index
  for (var index = 0; index < socketFiles.length; index++) {
    // Se obtiene el nombre del archivo
    let file = socketFiles[index];

    // Dentro de la colección del nuevo cliente, en la colección de archivos,
    // se crea el objeto con el nombre del archivo, y se asignan
    // permisos de owner
    TABLE[socketID].files[file] = {
      permissions: {},
      inUse: false,
      inUseBy: null
    };

    TABLE[socketID].files[file].permissions[socketID] = {
      write: true,
      read: true
    };
  }

  // Se recorre la lista de archivos de todos los clientes
  // para asignar los permisos aleatorios del nuevo cliente
  // y los de cada cliente ya existente dentro de los archivos del nuevo
  // Se recorre la lista clientsIDs
  for (var index = 0; index < clientsIDs.length; index++) {
    // Se obtiene el ID del cliente existente de la lista
    let clientID = clientsIDs[index];

    // Si el cliente no es el mismo cliente nuevo
    if (clientID !== socketID) {
      // Se obtienen los nombres de los archivos del cliente existente
      var files = Object.keys(TABLE[clientID].files);

      // Se recorre la lista de archivos del cliente existente
      // para asignar permisos aleatorios para el nuevo cliente
      for (var indexFiles = 0; indexFiles < files.length; indexFiles++) {
        // Se obtiene el nombre del archivo que se usa como llave
        let file = files[indexFiles];
        // Dentro de la colección del cliente existente, dentro de su colección
        // de archivos, se crea un nuevo objeto con los permisos del nuevo
        // cliente (Permisos aleatorios)
        TABLE[clientID].files[file].permissions[socketID] = getRandomPermissions();
      }

      // Se recorre la lista de los archivos del nuevo cliente, para
      // asginar los permisos del clienteExistente sobre los archivos
      // del nuevo cliente
      for (var indexFiles = 0; indexFiles < socketFiles.length; indexFiles++) {
        // Se obtiene el nombre del archivo que se usa como llave
        let file = socketFiles[indexFiles];
        // Dentro de la colección del cliente nuevo, dentro de su colección
        // de archivos, se crea un nuevo objeto con los permisos del cliente
        // ya existente (Permisos aleatorios)
        TABLE[socketID].files[file].permissions[clientID] = getRandomPermissions();
      }
    }
  }

  // Se retorna la colección principal hacía el servidor
  return TABLE;

};

module.exports = updatePermissionsFiles;
